class Students {
    constructor(name, age, date, gender, id, hobby) {
        this.name   = name;
        this.age    = age;
        this.date   = date;
        this.gender = gender;
        this.id     = id;
        this.hobby  = hobby;
    }
    panggil = () => { let sapa = (`Hello, ${this.name}`);
                            return sapa;
                        }
    
    setName = (newName) => { 
        this.name = newName;
        return this.panggil();
    }
    setAge = (newAge) => { 
        this.age = newAge;
        return this.age;
    }
    setDate = (newDate) => { 
        this.date = newDate;
        return this.date;
    }
    setGender = (newGender) => { 
        if (newGender == "Male" || newGender == "Female" || newGender == "GenderLess") {
            this.gender = newGender;
            return this.gender;
        }
        return this.gender;
    }
    addHobby = (hobby) => { 
        let hobbies = [this.hobby];
        hobbies.push(hobby);
        this.hobby = hobbies;
        return this.hobby;
    }
    addHobby = (hobby) => { 
        let hobbies = [this.hobby];
        hobbies.push(hobby);
        this.hobby = hobbies;
        return this.hobby;
    }
    removeHobby = (hobby) => {
        let hobbies = [this.hobby];
        let remove = hobbies.indexOf(hobby) +1;
        console.log(remove);
        this.hobby = hobbies.splice(remove, 1);
        return this.hobby;
    }
    getAll = () => {
        return `
        Name : ${this.name} 
        Age : ${this.age} 
        Date : ${this.date} 
        Gender : ${this.gender}
        Id Student : ${this.id} 
        Hobbies : ${this.hobby}
        `;
    }

}


const studentx = new Students("Jody", 21, "25 April 1999", "Genderless", "0092020026", "Singing");

console.log(studentx);
console.log(studentx.panggil());
console.log(studentx.setName("Mantap"));
console.log(studentx.setAge(17));
console.log(studentx.setDate("24 November 1999"));
console.log(studentx.setGender("Male"));
console.log(studentx.addHobby("Cooking"));
console.log(studentx.removeHobby("Singing"));
console.log(studentx);
studentx.getAll();