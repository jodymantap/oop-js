class Students {
    constructor(name, age, date, gender, id, hobby) {
        this.name   = name;
        this.age    = age;
        this.date   = date;
        this.gender = gender;
        this.id     = id;
        this.hobby  = hobby;
    }
    panggil = () => { let sapa = (`Hello, ${this.name}`);
                            return sapa;
                        }
}

//subclass
class TeknikKomputer extends Students {
    constructor(name, age, tools) {
        super(name, age);
        this.tools = tools;
    }
}

const studentx = new Students("Jody", 21, "25 April 1999", "Genderless", "0092020026", "Singing");

console.log(studentx.name);
console.log(studentx.panggil());

const jody = new TeknikKomputer("Jody", 12, "solder");
console.log(jody);
